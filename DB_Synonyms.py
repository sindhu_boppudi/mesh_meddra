from boto3.session import Session
import urllib.response
import urllib.request
import urllib.parse
import json
import re
import os
import errno

class DB_Indications():

    REST_URL = "http://data.bioontology.org"
    API_KEY = "6e00a808-51d9-4381-a593-787674286a98"
    search_terms = []
    search_terms_raw = []
    synonyms_dict = {}
    drug_name = ""
    source_name = ""
    file_name = ""

    bucket = 'nexscope-safety'
    # destination = 'DB_Synonyms_JSONS/'
    path = 'Sindhu_S3/'

    s3_root = os.path.join(
        "Sindhu_S3",
        "data",
        # datetime.datetime.now().strftime("%Y/%m/%d"),
        "2018/06/13",
        "enriched",
        "drugbank_indications"
    )

    def get_aws_session(self, new=False):
        aws_access_key_id = 'AKIAJLRXYJPUIWI6P3LA'
        aws_secret_access_key = '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'
        region_name = 'us-east-2'

        session = Session(
            aws_access_key_id=aws_access_key_id,
            aws_secret_access_key=aws_secret_access_key,
            region_name=region_name
        )

        return session

    def get_s3_client(self, new=False):
        session = self.get_aws_session(new)
        return session.client('s3')

    def assert_dir_exists(self, path):
        try:
            os.makedirs(path)
        except OSError as e:
            if e.errno != errno.EEXIST:
                raise

    def download_dir(self, client, bucket, path, target):
        if not path.endswith('/'):
            path += '/'
        paginator = client.get_paginator('list_objects_v2')
        for result in paginator.paginate(Bucket=bucket, Prefix=path):
            print(result)
            for key in result['Contents']:
                if "drugbank" in key["Key"]:
                    rel_path = key['Key'][len(path):]
                    if not key['Key'].endswith('/'):
                        actual_file = rel_path.rsplit('/', 1)[-1]
                        local_file_path = os.path.join(target, actual_file)
                        local_file_dir = os.path.dirname(local_file_path)
                        self.assert_dir_exists(local_file_dir)
                        client.download_file(bucket, key['Key'], local_file_path)

    def get_json(self, url):
        opener = urllib.request.build_opener()
        opener.addheaders = [('Authorization', 'apikey token=' + self.API_KEY)]
        url1 = url + "?include=all"
        return json.loads(opener.open(url1).read())

    def getontologyforfilter(self):
        return "MEDDRA"

    def get_db_synonyms(self):
        for result in self.search_terms:
            properties = self.get_json(self.REST_URL + "/search?q=" + urllib.parse.quote(result) + "&ontologies=" + self.getontologyforfilter() + "&longest_only=false&exclude_numbers=false&whole_word_only=true&exclude_synonyms=false")
            for label in properties["collection"]:
                label_req = label["@id"]
                final_split = re.split('[a-f/]+', label_req, flags=re.IGNORECASE)
                if final_split[-1] == result:
                    label_1 = self.get_json(label["links"]["self"])
                    synonyms_list = []
                    self.synonyms_dict[label_1["prefLabel"]] = []
                    synonyms_list.append({"Term_Notation": label_1["notation"]})
                    for res1, val in label_1["properties"].items():
                        if "classified_as" in res1:
                            for val1 in val:
                                val2 = val1.rsplit('/', 1)[-1]
                                res3 = label_1["links"]["self"]
                                res4 = re.split('[a-f]+', res3, flags=re.IGNORECASE)
                                res5 = res3.replace(res4[-1], val2)
                                res6 = self.get_json(res5)
                                notation = res6["notation"]
                                # synonyms_list.append({"Classified_as": res6["prefLabel"]})
                                synonyms_list.append({"Classified_as": res6["prefLabel"], "Classified_as_Notation": notation})
                                for res7, val in res6["properties"].items():
                                    if "classifies" in res7:
                                        for val1 in val:
                                            val2 = val1.rsplit('/', 1)[-1]
                                            res7 = label_1["links"]["self"]
                                            res8 = re.split('[a-f]+', res7, flags=re.IGNORECASE)
                                            res9 = res7.replace(res8[-1], val2)
                                            res10 = self.get_json(res9)
                                            notation10 = res10["notation"]
                                            synonyms_list.append({"Classifies": res10["prefLabel"], "Classifies_Notation": notation10})
                                            # synonyms_list.append({"Classifies_Notation": notation10})
                        elif "classifies" in res1:
                            for val1 in val:
                                val2 = val1.rsplit('/', 1)[-1]
                                res3 = label_1["links"]["self"]
                                res4 = re.split('[a-f]+', res3, flags=re.IGNORECASE)
                                res5 = res3.replace(res4[-1], val2)
                                res6 = self.get_json(res5)
                                notation = res6["notation"]
                                synonyms_list.append({"Classifies": res6["prefLabel"], "Classifies_Notation": notation})
                                # synonyms_list.append({"Classifies_Notation": notation})

                    self.synonyms_dict[label_1["prefLabel"]] = synonyms_list

        return self.synonyms_dict

    def get_download_location(self):
        current_directory = os.getcwd()
        download_location_path = os.path.join(current_directory, r'DB_Data/')
        if not os.path.exists(download_location_path):
            print("Making download directory")
            os.mkdir(download_location_path)

        self.download_path = download_location_path

        return self.download_path

    def write_to_s3(self):

        current_directory = os.getcwd()
        local_directory = os.path.join(current_directory, r'DB_Synonyms_JSONS/')
        client = self.get_s3_client()
        for root, dirs, files in os.walk(local_directory):
            for filename in files:
                if 'drugbank' in filename:
                    local_path = os.path.join(root, filename)
                    relative_path = os.path.relpath(local_path, local_directory)
                    s3_path = os.path.join(self.s3_root, relative_path)
                    client.upload_file(local_path, self.bucket, s3_path)


def main():

    db = DB_Indications()
    db.get_download_location()
    client = db.get_s3_client()
    db.download_dir(client, db.bucket, db.path, db.download_path)
    # db.get_search_terms()
    for filename in os.listdir(db.download_path):
        db.file_name = filename
        db.source_name = "Drug Bank"
        if filename.endswith(".json"):
            with open(db.download_path + "%s" % filename) as json_data:
                data = json.load(json_data)
                db.search_terms_raw = []
                if data["structured_indications"] is not None:
                    for ind in data["structured_indications"]["condition"]:
                        db.search_terms_raw.append(ind)

            db.search_terms = []

            for terms in db.search_terms_raw:
                term_split = re.split('[a-f/]+', terms, flags=re.IGNORECASE)
                term_split_1 = term_split[-1]
                db.search_terms.append(term_split_1)

        db.synonyms_dict = {}
        db.get_db_synonyms()

        current_directory = os.getcwd()
        directory = os.path.join(current_directory, r'DB_Synonyms_JSONS/')
        if not os.path.exists(directory):
            os.mkdir(directory)

        file_name_change = db.file_name.split('.')[0]
        file = os.path.join(directory, file_name_change + "_drugbank_indications.json")

        with open(file, 'w') as outfile:
            json.dump({"File_name": db.file_name,
                       "Source_Name": db.source_name,
                       "DB_Synonyms": db.synonyms_dict
                       }, outfile, indent=4)

    db.write_to_s3()

if __name__ == '__main__':
    main()

