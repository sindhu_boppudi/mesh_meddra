import urllib.response
import urllib.request
import urllib.parse
import json
import re
import unicodedata
import os


class MEDDRA_Parser():

    REST_URL = "http://data.bioontology.org"
    API_KEY = "6e00a808-51d9-4381-a593-787674286a98"
    result_url = []
    classes_terms = []
    search_results = []
    MEDDRA_terms = []
    classes_dict = dict()
    classified_as_dict = dict()
    classifies_dict = dict()
    result_dict = dict()
    file_name = ""
    source_name = ""
    my_dict_meddra = {}
    data = {}

    def get_json(self, url):
        opener = urllib.request.build_opener()
        opener.addheaders = [('Authorization', 'apikey token=' + self.API_KEY)]
        url1 = url + "?include=all"
        return json.loads(opener.open(url1).read())

    def print_annotations(self, annotations, classes_terms, get_class=True):
        # classes_terms = []
        self.classes_dict = {}
        for result in annotations:
            class_details = self.get_json(result["annotatedClass"]["links"]["self"]) if get_class else result["annotatedClass"]
            classes_terms.append(class_details)
            classes_notation = class_details["notation"]
            self.classes_dict[class_details["prefLabel"]] = classes_notation

    def print_properties(self, file_name, classes_terms):
        current_directory = os.getcwd()
        directory = os.path.join(current_directory, r'TEXT_OUTPUTS/')
        file_name_change = file_name.split('.')[0]
        file = os.path.join(directory, file_name_change + "_Meddra.txt")
        f1 = open(file, "w")
        self.my_dict_meddra= {}
        for result in classes_terms:
            f1.write("Term : " + result["prefLabel"])
            my_list_meddra = []
            self.my_dict_meddra[result["prefLabel"]] = []
            f1.write("\n")
            f1.write("Term Notation : " + result["notation"])
            my_list_meddra.append({"Term_Notation": result["notation"]})
            f1.write("\n")
            for res1, val in result["properties"].items():
                if "classified_as" in res1:
                    for val1 in val:
                        val2 = val1.rsplit('/', 1)[-1]
                        res3 = result["links"]["self"]
                        res4 = re.split('[a-f]+', res3, flags=re.IGNORECASE)
                        res5 = res3.replace(res4[-1], val2)
                        res6 = self.get_json(res5)
                        notation = res6["notation"]
                        f1.write("Classified as : " + res6["prefLabel"])
                        # my_list.append({"Classified_as": res6["prefLabel"]})
                        f1.write("\n")
                        f1.write("Classified as Notation : " + notation)
                        my_list_meddra.append({"Classified_as": res6["prefLabel"], "Classified_as_Notation": notation})
                        f1.write("\n")
                        for res7, val in res6["properties"].items():
                            if "classifies" in res7:
                                for val1 in val:
                                    val2 = val1.rsplit('/', 1)[-1]
                                    res7 = result["links"]["self"]
                                    res8 = re.split('[a-f]+', res7, flags=re.IGNORECASE)
                                    res9 = res7.replace(res8[-1], val2)
                                    res10 = self.get_json(res9)
                                    notation10 = res10["notation"]
                                    f1.write("Classifies : " + res10["prefLabel"])
                                    # my_list.append({"Classifies": res10["prefLabel"]})
                                    f1.write("\n")
                                    f1.write("Classifies Notation : " + notation10)
                                    my_list_meddra.append({"Classifies": res10["prefLabel"], "Classifies_Notation": notation10})
                                    f1.write("\n")
                elif "classifies" in res1:
                    for val1 in val:
                        val2 = val1.rsplit('/', 1)[-1]
                        res3 = result["links"]["self"]
                        res4 = re.split('[a-f]+', res3, flags=re.IGNORECASE)
                        res5 = res3.replace(res4[-1], val2)
                        res6 = self.get_json(res5)
                        notation = res6["notation"]
                        f1.write("Classifies : " + res6["prefLabel"])
                        f1.write("\n")
                        # my_list.append({"Classifies": res6["prefLabel"]})
                        f1.write("Classifies Notation : " + notation)
                        my_list_meddra.append({"Classifies": res6["prefLabel"], "Classifies_Notation": notation})
                        f1.write("\n")

            self.my_dict_meddra[result["prefLabel"]] = my_list_meddra

        return self.my_dict_meddra

    def getontologyforfilter(self):

        return "MEDDRA"

    def main(self, file_name, input_string, source_name, classes_terms):
        try:
            raw_text = input_string
            text_to_annotate = unicodedata.normalize('NFKD', raw_text).encode('ascii', 'ignore')
            annotations = self.get_json(self.REST_URL + "/annotator?text=" + urllib.parse.quote(text_to_annotate) + "&ontologies=" + self.getontologyforfilter() + "&longest_only=false&exclude_numbers=false&whole_word_only=true&exclude_synonyms=false?include=all")
            self.print_annotations(annotations, classes_terms)
            self.print_properties(file_name, classes_terms)

            current_directory = os.getcwd()
            directory = os.path.join(current_directory, r'JSON_OUTPUTS/')

            file_name_change = file_name.split('.')[0]
            file = os.path.join(directory, file_name_change + "_Meddra.json")

            with open(file, 'w') as outfile:
                json.dump({"File_name": file_name,
                           "Source_Name": source_name,
                           "MEDDRA_Terms": self.my_dict_meddra
                        }, outfile, indent=4)

        except urllib.request.HTTPError:
            current_directory = os.getcwd()
            directory = os.path.join(current_directory, r'JSON_OUTPUTS/')

            file_name_change = file_name.split('.')[0]
            file = os.path.join(directory, file_name_change + "_Meddra.json")

            with open(file, 'w') as outfile:
                json.dump({"File_name": file_name,
                           "Source_Name": source_name,
                           "MEDDRA_Terms": self.my_dict_meddra
                           }, outfile, indent=4)
            pass
        except Exception as e:
            print("Exception Occurred for {} with the error {}".format(file_name, e))
            pass
