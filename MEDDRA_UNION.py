from MEDDRA import MEDDRA_Parser
from MeSH_MEDD_Map import MeSH_MEDDRA_Mapping
import json
import os
from boto3.session import Session


class MEDDRA_Union():

    MEDDRA = MEDDRA_Parser()
    MeSH_MEDD_Map = MeSH_MEDDRA_Mapping()

    # bucket = 'nexscope-safety-va'
    # destination = 'JSON_OUTPUTS_1/'
    union_dict = {}

    def get_aws_session(self, new=False):
        aws_access_key_id = 'AKIAJLRXYJPUIWI6P3LA'
        aws_secret_access_key = '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'
        region_name = 'us-east-2'

        session = Session(
            aws_access_key_id=aws_access_key_id,
            aws_secret_access_key=aws_secret_access_key,
            region_name=region_name
        )

        return session

    def get_s3_client(self, new=False):
        session = self.get_aws_session(new)
        return session.client('s3')

    def main(self, file_name, source_name, classes_terms, mesh_link_list):
        meddra_terms = self.MEDDRA.print_properties(file_name, classes_terms)
        meddra_map_terms = self.MeSH_MEDD_Map.print_mappings(file_name, mesh_link_list)
        self.union_dict.clear()
        self.union_dict = meddra_map_terms

        if meddra_map_terms is not None:
            for term, val in meddra_terms.items():
                if term not in meddra_map_terms:
                    self.union_dict[term] = val
        else:
            self.union_dict = meddra_terms

        current_directory = os.getcwd()
        directory = os.path.join(current_directory, r'TEXT_OUTPUTS/')
        file_name_change = file_name.split('.')[0]
        file = os.path.join(directory, file_name_change + "_Final_Meddra.txt")
        f1 = open(file, "w")

        for key, val in self.union_dict.items():
            f1.write("Term : " + key)
            f1.write("\n")
            for i in val:
                for k, v in i.items():
                    if k == "Term_Notation":
                        f1.write("Term_Notation : " + v)
                        f1.write("\n")
                    if k == "Classified_as":
                        f1.write("Classified_as : " + v)
                        f1.write("\n")
                    if k == "Classified_as_Notation":
                        f1.write("Classified_as_Notation : " + v)
                        f1.write("\n")
                    if k == 'Classifies':
                        f1.write("Classifies : " + v)
                        f1.write("\n")
                    if k == "Classifies_Notation":
                        f1.write("Classifies_Notation : " + v)
                        f1.write("\n")

        current_directory = os.getcwd()
        directory = os.path.join(current_directory,  r'JSON_OUTPUTS/')

        file_name_change = file_name.split('.')[0]
        file = os.path.join(directory, file_name_change + "_Final_Terms.json")

        with open(file, 'w') as outfile:
            json.dump({"File_name": file_name,
                       "Source_Name": source_name,
                       "Final_Meddra_Terms": self.union_dict
                    }, outfile, indent=4)


