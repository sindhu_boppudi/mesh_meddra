from selenium import webdriver
from selenium.webdriver.common.action_chains import ActionChains
import time
import json
import os


class MeSH_Parser():

    options = webdriver.ChromeOptions()
    options.add_argument('--headless')
    options.add_argument('--disable-gpu')
    # MeSH_terms = []
    data = {}
    download_path = ""
    input_string = ""
    source_name = ""

    def call_website(self, file_name, input_string, source_name, MeSH_terms):

        actual_terms = []

        driver = webdriver.Chrome(chrome_options=self.options)
        driver.get('https://meshb.nlm.nih.gov/MeSHonDemand')

        text_area = driver.find_element_by_id('MODEntry')
        text_area.send_keys(input_string)
        driver.find_element_by_id('runMOD').click()

        time.sleep(60)

        for elem in driver.find_elements_by_xpath('.//span[@class = "highlightedModTerm modHighlight"]'):
            element_to_hover_over = elem
            hover = ActionChains(driver).move_to_element(element_to_hover_over)
            hover.perform()
            for term in driver.find_elements_by_xpath('.//a[@class = "highlightedModTerm hoveredModTerm ng-binding ng-scope redClass"]'):
                actual_terms.append(term.text)
                for term in actual_terms:
                    if term not in MeSH_terms:
                        MeSH_terms.append(term)

        current_directory = os.getcwd()
        directory = os.path.join(current_directory, r'JSON_OUTPUTS/')
        if not os.path.exists(directory):
            os.mkdir(directory)

        file_name_change = file_name.split('.')[0]
        file = os.path.join(directory, file_name_change + "_MeSH.json")

        with open(file, 'w') as outfile:
            json.dump({"File_name": file_name,
                       "Source_Name": source_name,
                       "MeSH_Terms": MeSH_terms
                    }, outfile, indent=4)

    def main(self, file_name, input_string, source_name, MeSH_terms):

        self.call_website(file_name, input_string, source_name, MeSH_terms)

        current_directory = os.getcwd()
        directory = os.path.join(current_directory, r'TEXT_OUTPUTS/')
        if not os.path.exists(directory):
            os.mkdir(directory)
        file_name_change = file_name.split('.')[0]
        file = os.path.join(directory, file_name_change + "_MeSH.txt")

        f1 = open(file, "w")

        for t in MeSH_terms:
            f1.write(t)
            f1.write("\n")


