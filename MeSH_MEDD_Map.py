import urllib.response
import urllib.request
import urllib.parse
import json
import re
import os


class MeSH_MEDDRA_Mapping():
    REST_URL = "http://data.bioontology.org"
    API_KEY = "6e00a808-51d9-4381-a593-787674286a98"

    mesh_link_list = []
    classes_terms = []
    mesh_terms = []
    mesh_terms_final = []
    mesh_links_final = []
    my_dict_map = {}

    def get_json(self, url):
        opener = urllib.request.build_opener()
        opener.addheaders = [('Authorization', 'apikey token=' + self.API_KEY)]
        url1 = url + "?include=all"
        return json.loads(opener.open(url1).read())

    def print_mappings(self, file_name, mesh_link_list):
        try:
            current_directory = os.getcwd()
            directory = os.path.join(current_directory, r'TEXT_OUTPUTS/')
            file_name_change = file_name.split('.')[0]
            file = os.path.join(directory, file_name_change + "_MeSH_MEDDRA.txt")
            f1 = open(file, "w")
            self.my_dict_map = {}
            for link in mesh_link_list:
                classes = self.get_json(self.REST_URL + '/search?q=' + urllib.parse.quote(link) + "&longest_only=false&exclude_numbers=false&whole_word_only=true&exclude_synonyms=false?include=all")
                for result in classes["collection"]:
                    if result["prefLabel"].lower() == link.lower():
                        search_data = result["links"]["mappings"]
                        if "MEDDRA" in search_data:
                            res2 = self.get_json(result["links"]["self"])
                            my_list_map = []
                            f1.write("Term : " + res2["prefLabel"])
                            f1.write("\n")
                            self.my_dict_map[res2["prefLabel"]] = []
                            f1.write("Term Notation : " + res2["notation"])
                            f1.write("\n")
                            my_list_map.append({"Term_Notation": res2["notation"]})
                            for res1, val in res2["properties"].items():
                                if "classified_as" in res1:
                                    for val1 in val:
                                        val2 = val1.rsplit('/', 1)[-1]
                                        res3 = res2["links"]["self"]
                                        res4 = re.split('[a-f]+', res3, flags=re.IGNORECASE)
                                        res5 = res3.replace(res4[-1], val2)
                                        res6 = self.get_json(res5)
                                        notation = res6["notation"]
                                        f1.write("Classified as : " + res6["prefLabel"])
                                        f1.write("\n")
                                        # my_list.append({"Classified_as": res6["prefLabel"]})
                                        f1.write("Classified as Notation : " + notation)
                                        f1.write("\n")
                                        my_list_map.append({"Classified_as": res6["prefLabel"], "Classified_as_Notation": notation})
                                        for res7, val in res6["properties"].items():
                                            if "classifies" in res7:
                                                for val1 in val:
                                                    val2 = val1.rsplit('/', 1)[-1]
                                                    res7 = res2["links"]["self"]
                                                    res8 = re.split('[a-f]+', res7, flags=re.IGNORECASE)
                                                    res9 = res7.replace(res8[-1], val2)
                                                    res10 = self.get_json(res9)
                                                    notation10 = res10["notation"]
                                                    f1.write("Classifies : " + res10["prefLabel"])
                                                    f1.write("\n")
                                                    # my_list.append({"Classifies": res10["prefLabel"]})
                                                    f1.write("Classifies Notation : " + notation10)
                                                    f1.write("\n")
                                                    my_list_map.append({"Classifies": res10["prefLabel"], "Classifies_Notation": notation10})
                                elif "classifies" in res1:
                                    for val1 in val:
                                        val2 = val1.rsplit('/', 1)[-1]
                                        res3 = res2["links"]["self"]
                                        res4 = re.split('[a-f]+', res3, flags=re.IGNORECASE)
                                        res5 = res3.replace(res4[-1], val2)
                                        res6 = self.get_json(res5)
                                        notation = res6["notation"]
                                        f1.write("Classifies : " + res6["prefLabel"])
                                        f1.write("\n")
                                        # my_list.append({"Classifies": res6["prefLabel"]})
                                        f1.write("Classifies Notation : " + notation)
                                        f1.write("\n")
                                        my_list_map.append({"Classifies": res6["prefLabel"], "Classifies_Notation": notation})

                            self.my_dict_map[res2["prefLabel"]] = my_list_map

        except urllib.request.HTTPError:
            pass
        except Exception as e:
            print("Exception Occurred for {} with the error {}".format(file_name, e))
            pass

            return self.my_dict_map

    def main(self, file_name, mesh_link_list):
        try:
            self.mesh_terms = []
            # self.mesh_link_list = []
            current_directory = os.getcwd()
            directory = os.path.join(current_directory, r'TEXT_OUTPUTS/')
            file_name_change = file_name.split('.')[0]
            file = os.path.join(directory, file_name_change + "_MeSH.txt")
            f = open(file, "r")
            self.mesh_terms = f.readlines()

            for term in self.mesh_terms:
                term2 = term.replace('\n', '')
                mesh_link_list.append(term2)

            self.print_mappings(file_name, mesh_link_list)

        except urllib.request.HTTPError:
            pass
        except Exception:
            pass



