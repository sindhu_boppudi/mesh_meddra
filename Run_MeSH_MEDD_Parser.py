from MeSH import MeSH_Parser
from MEDDRA import MEDDRA_Parser
from MeSH_MEDD_Map import MeSH_MEDDRA_Mapping
from MEDDRA_UNION import MEDDRA_Union
from boto3.session import Session
import os
import errno
import json
import urllib.error
import urllib.parse
import datetime
import logging


MeSH = MeSH_Parser()
MEDDRA = MEDDRA_Parser()
MeSH_MEDD_Map = MeSH_MEDDRA_Mapping()
MEDDRA_UNION = MEDDRA_Union()

bucket = 'nexscope-safety-va'
path = 'data/2018/06/05/'
data = {}
MeSH_terms = []
my_dict_meddra = {}
my_list_meddra = []
my_list_map = []
my_dict_map = {}
union_dict = {}
s3_root = os.path.join(
    # "Sindhu_S3",
    "data",
    # datetime.datetime.now().strftime("%Y/%m/%d"),
    "2018/06/05",
    "structured",
    "processed"
)

s3_root_1 = os.path.join(
    # "Sindhu_S3",
    "data",
    # datetime.datetime.now().strftime("%Y/%m/%d"),
    "2018/06/05",
    "enriched"
)

current_directory = os.getcwd()
log_file = os.path.join(current_directory, str(datetime.datetime.now().strftime("%Y-%m-%d")) + '_log_file.txt')


def get_logger(log_file):
    logging.basicConfig(filename=log_file, level=logging.DEBUG,
                        format='%(asctime)s \t[%(name)-s] \t%(filename)-s \t%(lineno)-d'
                               ' \t%(levelname)-s: \t%(message)s \r',
                        datefmt='%Y-%m-%dT%T%Z')
    logger = logging.getLogger(log_file)

    return logger


logger = get_logger(log_file)


def get_aws_session(new=False):
    aws_access_key_id = 'AKIAJLRXYJPUIWI6P3LA'
    aws_secret_access_key = '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'
    region_name = 'us-east-2'

    session = Session(
        aws_access_key_id=aws_access_key_id,
        aws_secret_access_key=aws_secret_access_key,
        region_name=region_name
    )

    return session


def get_s3_client(new=False):
    session = get_aws_session(new)
    return session.client('s3')


def assert_dir_exists(path):
    try:
        os.makedirs(path)
    except OSError as e:
        if e.errno != errno.EEXIST:
            raise


def download_dir(client, bucket, path, target):
    if not path.endswith('/'):
        path += '/'
    paginator = client.get_paginator('list_objects_v2')
    for result in paginator.paginate(Bucket=bucket, Prefix=path):
        for key in result['Contents']:
            # if "standardized" or "unstructured/processed" in key["Key"]:
            if "unstructured/processed" in key["Key"]:
                rel_path = key['Key'][len(path):]
                if not key['Key'].endswith('/'):
                    actual_file = rel_path.rsplit('/', 1)[-1]
                    local_file_path = os.path.join(target, actual_file)
                    local_file_dir = os.path.dirname(local_file_path)
                    assert_dir_exists(local_file_dir)
                    client.download_file(bucket, key['Key'], local_file_path)


def get_input_string(file_name):

    if 'pubmed' in file_name:
        input_string_title = data["article_title"]
        if "abstract_text" in data:
            for text in data["abstract_text"]:
                input_string_title = input_string_title + text['text']
            input_str = input_string_title
        else:
            input_str = input_string_title
    elif 'CN' in file_name:
        input_string_title = data["TI"]
        if "AB" in data:
            for text in data["AB"]:
                input_string_title = input_string_title + text
            input_str = input_string_title
        else:
            input_str = input_string_title
    elif 'who' in file_name:
        input_string_title = data["ti_en"]
        if "ab" in data:
            for text in data["ab"]:
                input_string_title = input_string_title + text
            input_str = input_string_title
        else:
            input_str = input_string_title
    else:
        if data["TI"]:
            for title in data["TI"]:
                input_string_title = title
                if "AB" in data:
                    input_string_title = input_string_title + data["AB"]
                    input_str = input_string_title
                else:
                    input_str = input_string_title
        else:
            if "AB" in data:
                input_string_title = data["AB"]
                input_str = input_string_title

    return input_str


def get_source_name(file_name):
    if 'pubmed' in file_name:
        source = "Pubmed"
    elif 'CN' in file_name:
        source = "Cochrane"
    elif 'who' in file_name:
        source = "WHO"
    else:
        source = "Unstructured"

    return source


def get_download_location():
    download_location_path = os.path.join(current_directory, r'Data/')
    if not os.path.exists(download_location_path):
        print("Making download directory")
        os.mkdir(download_location_path)

    return download_location_path


download_path = get_download_location()
client = get_s3_client()
download_dir(client, bucket, path, download_path)

count = 0
file3 = []

save_filename = os.path.join(current_directory, str(datetime.datetime.now().strftime("%Y-%m-%d")) + '_temp_file.txt')
try:
    for filename in os.listdir(download_path):
        count += 1
        logger.info('{}. Processing file {}'.format(str(count), filename))
        if os.path.exists(save_filename):
            save_file_data = open(save_filename, "r+")
        else:
            save_file_data = open(save_filename, 'a+')
        file1 = save_file_data.readlines()
        for line in file1:
            file2 = line.rstrip()
            file3.append(file2)
        if filename not in file3:
            file_name = filename
            with open(download_path + "%s" % file_name) as json_data:
                data = json.load(json_data)
            input_string = get_input_string(file_name)
            source_name = get_source_name(file_name)
            MeSH_terms = []
            classes_terms = []
            mesh_link_list = []
            MeSH.main(file_name, input_string, source_name, MeSH_terms)
            MEDDRA.main(file_name, input_string, source_name, classes_terms)
            MeSH_MEDD_Map.main(file_name, mesh_link_list)
            MEDDRA_UNION.main(file_name, source_name, classes_terms, mesh_link_list)
            save_file_data.write(file_name)
            file1.append(file_name)
            save_file_data.write("\n")
            save_file_data.close()
        elif filename in file1:
            pass

except KeyError:
    pass
except TypeError:
    pass
except AttributeError:
    pass
except NameError:
    pass
except urllib.error.HTTPError:
    pass
except Exception as e:
    logger.info("Exception Occurred with the error {}".format(e))
    pass


def write_to_s3():
    local_directory = os.path.join(current_directory, r'JSON_OUTPUTS/')
    # client = get_s3_client()
    for root, dirs, files in os.walk(local_directory):
        for filename in files:
            if 'MeSH' in filename:
                local_path = os.path.join(root, filename)
                relative_path = os.path.relpath(local_path, local_directory)
                if "pubmed" in relative_path:
                    s3_path = os.path.join(s3_root, "pubmed", "enriched", "mesh", relative_path)
                    client.upload_file(local_path, bucket, s3_path)
                elif "CN" in relative_path:
                    s3_path = os.path.join(s3_root, "cochrane", "enriched", "mesh", relative_path)
                    client.upload_file(local_path, bucket, s3_path)
                elif "who" in relative_path:
                    s3_path = os.path.join(s3_root, "who", "enriched", "mesh", relative_path)
                    client.upload_file(local_path, bucket, s3_path)
                else:
                    s3_path = os.path.join(s3_root_1, "mesh", relative_path)
                    client.upload_file(local_path, bucket, s3_path)
            elif 'Meddra' in filename:
                local_path = os.path.join(root, filename)
                relative_path = os.path.relpath(local_path, local_directory)
                if "pubmed" in relative_path:
                    s3_path = os.path.join(s3_root, "pubmed", "enriched", "meddra", relative_path)
                    client.upload_file(local_path, bucket, s3_path)
                elif "CN" in relative_path:
                    s3_path = os.path.join(s3_root, "cochrane", "enriched", "meddra", relative_path)
                    client.upload_file(local_path, bucket, s3_path)
                elif "who" in relative_path:
                    s3_path = os.path.join(s3_root, "who", "enriched", "meddra", relative_path)
                    client.upload_file(local_path, bucket, s3_path)
                else:
                    s3_path = os.path.join(s3_root_1, "meddra", relative_path)
                    client.upload_file(local_path, bucket, s3_path)
            elif 'Final' in filename:
                local_path = os.path.join(root, filename)
                relative_path = os.path.relpath(local_path, local_directory)
                if "pubmed" in relative_path:
                    s3_path = os.path.join(s3_root, "pubmed", "enriched", "mapped", relative_path)
                    client.upload_file(local_path, bucket, s3_path)
                elif "CN" in relative_path:
                    s3_path = os.path.join(s3_root, "cochrane", "enriched", "mapped", relative_path)
                    client.upload_file(local_path, bucket, s3_path)
                elif "who" in relative_path:
                    s3_path = os.path.join(s3_root, "who", "enriched", "mapped", relative_path)
                    client.upload_file(local_path, bucket, s3_path)
                else:
                    s3_path = os.path.join(s3_root_1, "mapped", relative_path)
                    client.upload_file(local_path, bucket, s3_path)


write_to_s3()
os.remove(save_filename)




